export const getImages = async () => {
    const url = `https://notflixtv.herokuapp.com/api/v1/movies`
  
    try {
      const response = await fetch(url, {
        method: "GET",
      });
      return response.json();
    } catch (error) {
      console.log(error);
      throw error;
    }
  };
  
//   export const MovieDetails = async () => {
//     const url =
//       "";
  
//     try {
//       const response = await fetch(url, {
//         method: "GET",
//       });
//       return response.json();
//     } catch (error) {
//       console.log(error);
//       throw error;
//     }
//   };
  

