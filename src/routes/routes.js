import React from "react";
import Home from "../Pages/Home/Home";
import MovieDetails from "../components/MovieDetails/MovieDetails";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Layout from "../components/Layout/Layout";


const Routes = () => {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/movie/details">
            <MovieDetails />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
};

export default Routes;
