import React from "react";
import MovieDetails from "../components/MovieDetails/MovieDetails";


const OverviewPage = () => {
  return (
    <div>
        <MovieDetails />
    </div>
  );
};

export default OverviewPage;
