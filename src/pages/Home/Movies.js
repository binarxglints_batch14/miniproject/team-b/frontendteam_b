import { useEffect, useState } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";
import "./Movies.css";

function Movies() {
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    axios
      .get(
        "https://notflixtv.herokuapp.com/api/v1/movies?page=1&limit=15"
      )
      .then((res) => {
        console.log(res.data.data.docs);
        setMovie(res.data.data.docs);
      });
  }, []);

  return (
    <>
    <div className="movieList">
      {movie.map((movie) => (
        <div className="movie" key={movie.id}>
          <Link className="movie-link" to={`/movie/details/${movie.id}`}>
          <img src={`https://image.tmdb.org/t/p/original/${movie.poster}`}
               alt="img"
               />
            <h3>{movie.title}</h3>
            <h3>{movie.genres[0]}</h3>
          </Link>
        </div>
      ))}
    </div>
    </>
  );
}

export default Movies;


export function TabContainer(props) {
  const param = useParams()

  useEffect(() => {
    props.handleFilter(param)
  }, [param])

  console.log(param)
  return (
    <>
    <div>{props.children}</div>
    </>
  )
}