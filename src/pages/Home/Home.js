import React from "react";
import CarouselContainer from "../../components/carousel/CarouselContainer";
import Footer from "../../components/Footer/Footer";
import MovieList from "../../components/MovieList/Movielist";
import Category from "../../components/category/Category";
//import { useState } from 'react';
import "./Home.css";

const Home = () => {

  return (
    <>
      <CarouselContainer />
      {/* <div className="category">
        <h4>Browse by category</h4>
        <div className="category-button">
          <button>All</button>
          <button>Anime</button>
          <button>Action</button>
          <button>Adventure</button>
          <button>Science Fiction</button>
          <button>Comedy</button>
        </div>
      </div> */}
      <Category />
      <MovieList />
    </>
  );
};

export default Home;
