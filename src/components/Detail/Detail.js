import Overview from "../overview/Overview";
import Characters from "../characters/Characters";
import Reviews from "../reviews/Reviews";

export const Detail = (props) => {
  switch (props.click) {
    case 3:
      return <Reviews />;
    case 2:
      return <Characters />;
    default:
      return <Overview />;
  }
};
