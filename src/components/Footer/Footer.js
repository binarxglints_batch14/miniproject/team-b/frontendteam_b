import React from "react";
import './Footer.css';

import logo from '../../assets/logo.png';
import apple from '../../assets/apple.png';
import google from '../../assets/google.png';
import fb from '../../assets/fb.png';
import pinterest from '../../assets/pinterest.png';
import ig from '../../assets/ig.png';

const Footer = () => {
    return (
        <div>
        <div className="footer">
            <div className="container">
                <div className="row">
                    <div className="logo">  
                        <div className="name">
                        <img
                            src={logo}
                            alt="milantv"
                        />
                        <h3>Milan TV</h3>
                        </div>
                    <div className="isi">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                    </div>
                    </div>
                    <div className="info">
                        <ul> 
                            <li>Tentang Kami</li>
                            <li>Blog</li>
                            <li>Layanan</li>
                            <li>Karir</li>
                            <li>Pusat Media</li>
                        </ul>
                    </div>
                    <div className="link">
                    <div className="download">
                        <h3>Download</h3>
                        <img
                            src={apple}
                            alt="apple store"
                            />
                        <img
                            src={google}
                            alt="google play"
                            />
                    </div>
                    <div className="social-media">
                        <h3>Social Media</h3>
                        <img
                            src={fb}
                            alt="facebook"
                            />
                        <img
                            src={pinterest}
                            alt="pinterest"
                            />
                        <img
                            src={ig}
                            alt="instagram"
                            />
                    </div>
                    </div>
                    </div>
                <div className="copyright">
                        <p>Copyright © 2000-202 MilanTV. All Rights Reserved</p>
    
                </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;
