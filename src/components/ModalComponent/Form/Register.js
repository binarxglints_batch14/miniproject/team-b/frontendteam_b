import React from "react";
import './Form.css'

export const RegisterForm = ({ onSubmit, changeFormType }) => {
  return (
    <form onSubmit={onSubmit}>
      <div className="login-title">Dinsey Coolstar+</div>
      <br />
      <div className="form-group">
        <label htmlFor="name">Full Name</label>
        <input className="form-control input" id="name" />
      </div>
      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input type="email" className="form-control input" id="email" />
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input type="password" className="form-control" id="password" />
      </div>
      <div className="form-group">
        <button
          className="btn btn-primary form-control"
          type="submit">
          Sign Up
        </button>
      </div>
      <div className="footer-text">
        Already have an Account? <span onClick = {() => changeFormType('login')}>Sign In</span>
      </div>
    </form>
  );
};
export default RegisterForm;
