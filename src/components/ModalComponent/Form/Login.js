import React from "react";
import './Form.css'

export const LoginForm = ({ onSubmit, changeFormType }) => {
  return (
    <form onSubmit={onSubmit}>
      <div className="login-title">Dinsey Coolstar+</div>
      <br />
      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input type="email" className="form-control input" id="email" />
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input type="password" className="form-control" id="password" />
      </div>
      <div className="form-group">
        <button
          className="btn btn-primary form-control"
          type="submit">
          Sign In
        </button>
      </div>
      <div className="footer-text">
        Don't have an Account? <span onClick = {() => changeFormType('register')} >Sign Up</span>
      </div>
    </form>
  );
};
export default LoginForm;
