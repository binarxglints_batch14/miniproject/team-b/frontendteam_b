import axios from "axios";
import { useEffect} from "react";
import {Chip} from "@material-ui/core";

const Genres = ({
    selectedGenres,
    setSelectedGenres,
    genres,
    setGenres,
    type,
    setPage,
}) => {

    const handleAdd = (genre) => {
        setSelectedGenres([...selectedGenres, genre])
        setGenres(genres.filter((g) => g.id !== genre.id))
        setPage(1)
    }

    const handleRemove = (genre) => {
        setSelectedGenres(
            selectedGenres.filter((selected) => selected.id !== genre.id)
        )
        setGenres([...genres, genre])
        setPage(1)
    }
    
    const fetchGenres = async() => {
        const { data } = await axios.get(
            "https://notflixtv.herokuapp.com/api/v1/movies/genres"
            )

            setGenres(data.genres)
            }

            console.log(genres)

            useEffect(() => {
                fetchGenres()

                return () => {
                  setGenres({})
                }
                
            }, [])

            return <div style={{ padding: "6px 0"}}>
                {selectedGenres &&
                    genres.map((genre) => (
                        <Chip 
                        label={genre.name} 
                        style={{margin: 2}} 
                        size='small'
                        color='primary' 
                        key={genre.id} 
                        clickable
                        onDelete={()=> handleRemove(genre)}
                        />
                ))}
                {genres && 
                    genres.map((genre) => (
                        <Chip 
                        label={genre.name} 
                        style={{margin: 2}} 
                        size='small' 
                        key={genre.id} 
                        clickable
                        onClick={()=>handleAdd(genre)}
                        />
                    
                ))}
            </div>
}

export default Genres;




// import React, { useCallback } from "react";
// import axios from "axios";
// import { ClassNames } from "@emotion/react";
// import { style } from "@mui/system";

// const Genres = ({genre, onGenre}) => {
//     const [isActive, seActive] = useState(false);
//     const [genres, setGenres] = useState([]);

//     const selectGenre = useCallback (e => {
//         onGenre(e.target.innerHTML)
//         setActive(genre);
//         console.log('aktif', isActive);
//     }, [onGenre])

//     useEffect(() => {
//         axios
//         .get(`https://notflixtv.herokuapp.com/api/v1/movies/genres`)
//         .then((res) => {
//             setGenres(res.data.data)
//         });
//     }, []);
 
      
//     return (
//         <div>
//             <button onClick={(e) => selectGenre(e)}>All</button>
//             {genres.map((genres) => (
                
//             ))}
//         </div>
//     )
// }

// export default Genres;
