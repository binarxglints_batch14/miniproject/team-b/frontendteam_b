import React, { useEffect, useState } from "react";
import { getImages } from "../services/Services";


const Character = () => {
  const [actors, setActor] = useState([]);

  useEffect(() => {
    getImages()
      .then((response) => {
        setActor(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  console.log(actors);
  return (
    <div className="card-containers">
     {actors.casts.map(cast => <h2 key={cast.id}>{cast.name}</h2>
     
      )}
    </div>
  );
};

export default Character;
