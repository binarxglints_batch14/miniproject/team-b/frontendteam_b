import axios from "axios";
import { useEffect, useState } from "react";
import MovieCard from "../MovieCard/MovieCard";
import { Container, Row, Col } from "react-bootstrap";

export default function MovieList() {
  const [data, setData] = useState([]);
  useEffect(() => {
    axios
      .get("https://notflixtv.herokuapp.com/api/v1/movies?limit=12")
      .then((response) => {
        setData(response?.data?.data?.docs);
      });
  }, []);
  console.log(data);
  return (
    <>
      <Container>
        <Row>
          {data?.map((movie) => (
            <Col xs={3}>
              <MovieCard key={movie._id} singleMovie={movie} />
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}
