import "bootstrap/dist/css/bootstrap.min.css";
import { Navbar, Nav, Form, FormControl } from "react-bootstrap";
import styles from "./navbars.module.css";
import logo from "./logo.png";

function Navbars({ showModal }) {
  console.log(showModal);
  return (
    <div className={styles.Navbar}>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className={styles.logoMilan}>
          <div className={styles.logo}>
            <img src={logo} alt="logo" />
          </div>
        </div>
        <div className={styles.logoText}>
          <Navbar.Brand href="#">Milan TV</Navbar.Brand>
        </div>
        <Form className="d-flex" id={styles.dflex}>
          <FormControl
            type="search"
            placeholder="search movie"
            className="mr-2"
            aria-label="Search"
          />
        </Form>
        <div className={styles.signInLink} onClick={showModal}>
          Sign in
        </div>
      </nav>
    </div>
  );
}

export default Navbars;
