import React from 'react';


import './Category.css';


const Category = () => {

    return (
        <>
        <div className="category">
            <h4>Browse by category</h4>
            <div className="category-button">
                <button>All</button>
                <button>Anime</button>
                <button>Action</button>
                <button>Adventure</button>
                <button>Science Fiction</button>
                <button>Comedy</button>
            </div>
        </div>
        
        </>
    );
};

export default Category;
