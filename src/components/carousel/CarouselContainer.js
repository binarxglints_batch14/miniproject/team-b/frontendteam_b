import React from 'react';
import { Carousel } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Carousel.css';

import Img1 from '../../assets/Img1.jpg';
import Img2 from '../../assets/Img2.jpg';



function CarouselContainer() {
  return (

    <Carousel>
    <Carousel.Item style={{height: "500px"}}>
      <img
        className="d-block w-100"
        src={Img1}
        alt=""
      />
    </Carousel.Item>
    <Carousel.Item style={{height: "500px"}}>
      <img
        className="d-block w-100"
        src={Img2}
        alt="Second slide"
      />
    </Carousel.Item>
    <Carousel.Item style={{height: "500px"}}>
      <img
        className="d-block w-100"
        src="https://i2.wp.com/en.blog.kkday.com/wp-content/uploads/kr_Start-Up-poster-tvN.jpg?resize=924%2C485&amp;ssl=1"
        alt="Third slide"
      />
    </Carousel.Item>
  </Carousel>
  )
}

export default CarouselContainer;