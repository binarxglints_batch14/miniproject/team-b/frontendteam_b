export default function MovieCard({ singleMovie }) {
  console.log(singleMovie);
  return (
    <div>
      <img src={`https://image.tmdb.org/t/p/original${singleMovie.poster}`} />
      <h3>{singleMovie.title}</h3>
    </div>
  );
}
